#!/usr/bin/python
# Purpose  : Script to automatically check new Gentoo rsync and distfile mirrors
# Author(s): Shyam Mani <fox2mike@gentoo.org>
#          : Arun Raghavan <ford_prefect@gentoo.org>
#          : Robin H. Johnson <robbat2@gentoo.org>
# Created  : 05 Jul 2008

# Dedicated to robbat2 aka Robin, for sparking off the idea ;)

import sys
import time
import os
import datetime
import subprocess
import logging
import urllib2
import csv

import check

rmaxlag = 45 * 60
dmaxlag = 5.5 * 3600
relmaxlag = 2 * 3600 * 24

HOME = os.getenv('HOME')
reportpath = HOME + "/gard/reports/"
bugurl = "http://bugs.gentoo.org/buglist.cgi?query_format=advanced&product=Mirrors&status_whiteboard_type=allwordssubstr&status_whiteboard=due&bug_status=IN_PROGRESS&columnlist=bug_id,opendate,bug_status,resolution,short_desc,status_whiteboard&ctype=csv"

def start_logging(url, bugnum, proto):
	logger = logging.getLogger()
	logger.setLevel(logging.INFO)

	logformat = '%(levelname)s\t%(message)s'
	format = '%s\t%s\t%s' % (bugnum, proto, logformat)
	formatter = logging.Formatter(format)

	console = logging.StreamHandler(sys.stdout)
	console.setFormatter(formatter)
	logger.addHandler(console)

	file = logging.FileHandler(reportpath + url.replace('/', '-'))
	file.setFormatter(formatter)
	logger.addHandler(file)

	return (console, file)

def stop_logging(handlerss):
	for handler in handlers:
		logging.getLogger().removeHandler(handler)

# See if we got a specific bug number to check
if (len(sys.argv) == 2):
	bugurl += "&bug_id=" + sys.argv[1]

# TODO: Add a timeout
_ = urllib2.urlopen(bugurl)
file = _.readlines()
_.close()

# Format of CSV = 234083,"2008-08-06 13:01:32","ASSIGNED",,"[rsync,distfiles|all] New mirror Cambrium BV (The Netherlands)","due:2008/09/01;dist:http://mirror.cambrium.nl/pub/os/linux/gentoo/|ftp://mirror.cambrium.nl/pub/os/linux/gentoo/|rsync://mirror.cambrium.nl/gentoo/;portage:mirror.cambrium.nl"

reader = csv.reader(file, delimiter=',', quotechar='"')
for data in reader:
	bugnum=data[0]
	swboard=data[5]
	if bugnum == "bug_id":
		continue
	else:
		details=swboard.split(';')
		duedate=details[0]
		duedate=duedate.replace("\"","")
		#print duedate
		urls=details[1].replace("dist:","")
		port=details[2].split(':')
		grsync=port[1].replace("\"","")
		temp=urls.split('|')
		http=temp[0]
		ftp=temp[1]
		drsync=temp[2]
		#print http
		#print ftp
		#print drsync
		#print grsync
		if len(http) > 0:
			boo=http.split('/')
			addy=boo[2]
		elif len(ftp) > 0:
			boo=ftp.split('/')
			addy=boo[2]
		else:
			addy=grsync
		#hdomain='/'+addy[2]
		if len(grsync) > 0:
			if os.path.isdir(reportpath):
				pass
	        	else:
        	        	os.makedirs(reportpath)

			handlers = start_logging(addy, bugnum, 'rsync')

			info4=grsync+" wants to be a gentoo-portage mirror and is being checked as per Bug #"+bugnum
			logging.info(info4)
			prsync = 'rsync://%s/' % grsync

			if check.PortageCheck(prsync).check(rmaxlag) is True:
				logging.info(addy + ' is in sync')
			else:
				logging.info(addy + ' is out of sync')

			bugurl1 = "Bug URL : https://bugs.gentoo.org/"+bugnum+" , "+duedate
			logging.info(bugurl1)

			# New line for the prettiness
			logging.info('')
			stop_logging(handlers)
		if len(http) > 0:
			if os.path.isdir(reportpath):
      				pass
        		else:
       				os.makedirs(reportpath)
			handlers = start_logging(addy, bugnum, 'http ')
			info5 = addy+" wants to be source mirror and is being checked as per Bug #"+bugnum+ " over http"
			logging.info(info5)

			synced = True
			if check.DistfilesCheck(http).check(dmaxlag) is not True:
				synced = False
			if check.ReleasesCheck(http).check(relmaxlag) is not True:
				synced = False
			if synced:
				logging.info(addy + ' is in sync')
			else:
				logging.info(addy + ' is out of sync')

			bugurl1 = "Bug URL : https://bugs.gentoo.org/"+bugnum+" , "+duedate
			logging.info(bugurl1)

			# New line for the prettiness
			logging.info('')
			stop_logging(handlers)
		if len(ftp) > 0:
			if os.path.isdir(reportpath):
       				pass
       			else:
       				os.makedirs(reportpath)

			handlers = start_logging(addy, bugnum, 'ftp  ')

			info6 = addy+" wants to be a source mirror and is being checked as per Bug #"+bugnum+ " over ftp"
			logging.info(info6)

			synced = True
			if check.DistfilesCheck(ftp).check(dmaxlag) is not True:
				synced = False
			if check.ReleasesCheck(ftp).check(relmaxlag) is not True:
				synced = False
			if synced:
				logging.info(addy + ' is in sync')
			else:
				logging.info(addy + ' is out of sync')

			bugurl1 = "Bug URL : https://bugs.gentoo.org/"+bugnum+" , "+duedate
			logging.info(bugurl1)

			# New line for the prettiness
			logging.info('')
			stop_logging(handlers)
		if len(drsync) > 0:
			# Replace with check.DistfilesCheck(drsyync).check()
			# after testing
			if os.path.isdir(reportpath):
	       			pass
      			else:
               			os.makedirs(reportpath)

			handlers = start_logging(addy, bugnum, 'rsync')

			info7 = addy+" wants to be a source mirror and is being checked as per Bug #"+bugnum+ " over rsync"
			logging.info(info7)

			if check.DistfilesCheck(drsync).check(dmaxlag) is True:
				logging.info(addy + ' is in sync')
			else:
				logging.info(addy + ' is out of sync')

			# Now common lines
			bugurl7 = "Bug URL : https://bugs.gentoo.org/"+bugnum+" , "+duedate
			logging.info(bugurl7)

			# New line for the prettiness
			logging.info('')
			stop_logging(handlers)
